/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package featurs;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Parastoo
 */
public class F02_Similarity extends Feature{
    
    public F02_Similarity(String dataset, String rs, int docCount, int start, int end, String feature) {
        super(dataset, rs, docCount, start, end, feature);
    }

    @Override
    public void run() throws IOException, SQLException{
        
            
            createEntitiyTitle();
            createCollection();
            for (int i =start; i<=end; i++) {
                p_q_d_ForALLRelatedDocs = new HashMap<>();
                p_q_d_Down_ForALLRelatedDocs = new HashMap<>();
                CalculateSimilarityScore(Integer.toString(i));

            }
            resultwriter.close();
        
    }
    
    private double CalculateSimilarityScore(String queryId) {
        double score = 0;
        try {
            ArrayList<String> docsTitle = getRelatedDocsToQuery(queryId);
            HashSet<String> TitlesOfQueryEntities = getTitlesOfQueryEntities(queryId);
            
            double p_q_D = calculateP_q_D(TitlesOfQueryEntities,docsTitle);
            score = p_q_D;
               
            predictions.put(queryId,score);
            resultwriter.write(queryId+ " "+ score);
            resultwriter.write("\n");
            
            resultwriter.flush();
        } 
        catch (Exception ex) {
            
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       return score;
    
    }

    
    
}