/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package featurs;

import be.vanoosten.esa.tools.ESAtester_EN;
import com.github.andrewoma.dexx.collection.Pair;
import core.SqlConnection;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import core.SerializeHashMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import org.apache.lucene.queryparser.classic.ParseException;

/**
 *
 * @author parastoo
 */
public class Feature {
    public final String dataset ;
    public final String rs;
    public final int docCount;
    public final int start;
    public final int end;
    public BufferedWriter resultwriter;
    public String filename;
    public SerializeHashMap SerializeHashMap;
    public int publicvectorSize = 10;
    public int privatevectorSize = 100;
    public double p_e_C_Down = 0;
    public int collectionFrequency;
    
    public HashSet<Pair<String,String>> queryDocRelatedness = new HashSet<>();
    public HashMap<String, Double> predictions = new HashMap<>();
    public HashMap<String,Integer> collection  = new HashMap<>();
    public static HashMap<String,String> entityTitle = new HashMap<>() ;
    public HashMap<String , HashMap<String,Integer>> docs = new HashMap<>();
    public HashMap<String , HashMap<String,Integer>> docsrelated = new HashMap<>();
    public HashMap<String,String> queriesEntity = new HashMap<>();
    public HashMap<String,String> queriesTitle = new HashMap<>();
    public Map<String ,Map<String,Float>> colelementVectors = new HashMap<>();
    public Map<String ,Map<String,Float>> colelementSpecialVectors = new HashMap<>();
    public Map<String,Double> p_q_d_ForALLRelatedDocs = new HashMap<>();
    public Map<String,Double> p_q_d_Down_ForALLRelatedDocs = new HashMap<>();
    public HashMap<String, Double> p_e_C_up_ForALLEntities = new HashMap<>();
    public HashMap<String,String> misleadedDoc_ForALLQueries = new HashMap<>();
    public HashMap<Pair<String,String>,Float> docScore = new HashMap<>();
    public ESAtester_EN ee = new ESAtester_EN();
    
    
    public Feature(String dataset ,String rs ,int docCount, int start,int end,String feature){
        this.dataset = dataset;
        this.rs = rs;
        this.docCount = docCount;
        this.start = start;
        this.end = end;
        try {
            this.filename= "./crossfold/"+dataset+"/result/"+this.rs+"-"+dataset+"-"+this.docCount+"docs-percentEntities-F"+feature+".txt";
            File f = new File(filename);
            resultwriter = new BufferedWriter(
                    new FileWriter(f));
        } catch (IOException ex) {
        }
        this.SerializeHashMap = new SerializeHashMap(dataset, rs, docCount, start, end);
    }
    
    public void run() throws IOException, SQLException{}
    
    protected void createEntitiyTitle() throws SQLException {
        if(entityTitle.size() == 0){
            entityTitle = new HashMap<>();
            String  query = "SELECT entity, title from entitytitle";
            ResultSet resultset2 = SqlConnection.select(query);


            while(resultset2.next()){

                String entity = resultset2.getString(1);
                String title  = resultset2.getString(2);

                if(!entityTitle.containsKey(entity))
                    entityTitle.put(entity, title);

            }
        }
    }

    protected HashMap<String, Integer> createTopEntitiesRelatedToDoc(String docTitle) {
        HashMap<String, Integer> result = new HashMap<>();
        HashMap<String, Integer> dochash = docs.get(docTitle);
        for(String et : dochash.keySet()){
            try {
                Map<String,Float> v;
                if(!colelementVectors.containsKey(et)){
                    v = SerializeHashMap.deserializeHashMap(et);
                    colelementVectors.put(et, v);
                    
                }
                else{
                    v = colelementVectors.get(et);
                }
                for(String e: v.keySet()){
                    if(result.containsKey(e))
                        result.put(e, result.get(e)+1);
                    else
                        result.put(e, 1);
                }
            } catch (Exception ex) {
                System.out.println(et+" has a problem in create top entities");
            }
        }
        for(String e: dochash.keySet()){
                    if(result.containsKey(e))
                        result.put(e, result.get(e)+dochash.get(e));
                    else
                        result.put(e, dochash.get(e));
        }
        return result;
    }
    
    protected HashMap<String, Integer> createHashMap(String doc) {
        String[] entities = doc.split(",");
        HashMap<String , Integer> result = new HashMap<>();
        for(String entity : entities){
            if(entityTitle.containsKey(entity)){
                String title = entityTitle.get(entity);
                if(!result.containsKey(title))
                    result.put(title, 1);
                else
                    result.put(title, result.get(title)+1);
                if(!collection.containsKey(title))
                    collection.put(title, 1);
                else
                    collection.put(title, collection.get(title)+1);
            }
        }
        return result;
    }
    
    protected void createCollection() throws SQLException {
        String query = "SELECT q.id,q.entity,d.title, d.entity,rs.document_id,rs.score,q.title,rs.rank from "+rs+"_"+dataset+" rs left join document_"+dataset+
                        " d on rs.document_id = d.title " +
                        "left join query_"+dataset+" q on rs.query_id = q.id" +
                        " where q.id  is not null and q.id <= "+end+" and  rs.rank <= "+docCount; 
        ResultSet resultset = SqlConnection.select(query);
        while(resultset.next()){
            if(resultset.getString(4)!= null){
                if(!docs.containsKey(resultset.getString(3)))
                {
                    HashMap<String,Integer> dochash = createHashMap(resultset.getString(4));
                    docs.put(resultset.getString(3),dochash);
                    docsrelated.put(resultset.getString(3),createTopEntitiesRelatedToDoc(resultset.getString(3)));
                }

                queryDocRelatedness.add(new Pair<String,String>(resultset.getString(1),resultset.getString(3)));
                queriesEntity.put(resultset.getString(1), resultset.getString(2));
                queriesTitle.put(resultset.getString(1), resultset.getString(7));
                docScore.put(new Pair<String,String>(resultset.getString(1),resultset.getString(3)),Float.parseFloat( resultset.getString(6)));
            }
            
        }
        for (String entity :collection.keySet()) {
                collectionFrequency += collection.get(entity);
        }
    }

    protected HashMap<String, Float> getEntityVector(String entity, int size) throws IOException, ParseException {
        Map<String,Float> v;
        HashMap<String,Float> eRelated = new HashMap<>();
        try{
            if(size == privatevectorSize){
                if(!colelementSpecialVectors.containsKey(entity)){
                    v = SerializeHashMap.deserializeSpecialHashMap(entity);
                    v = reduceHashmapSize(v, publicvectorSize);
                    colelementSpecialVectors.put(entity, v);
                }
                else{
                    v = colelementSpecialVectors.get(entity);
                }
            }
            else{
                if(!colelementVectors.containsKey(entity)){
                    v = SerializeHashMap.deserializeHashMap(entity);
                    v = reduceHashmapSize(v, publicvectorSize);
                    colelementVectors.put(entity, v);
                }
                else{
                    v = colelementVectors.get(entity);
                }
            }
            for(String e: v.keySet()){
                eRelated.put(e, v.get(e));
            }
            eRelated.put(entity,(float) 1);
        }
        catch(Exception ex){
            System.out.println(entity+" with size of " + size + " has a problem");
        }
        return eRelated;
    }
    
    protected LinkedHashMap<String, Float> reduceHashmapSize( Map<String, Float> passedMap, double size){
        List<String> mapKeys = new ArrayList<>(passedMap.keySet());
        List<Float> mapValues = new ArrayList<>(passedMap.values());
        Collections.sort(mapKeys, Collections.reverseOrder());
        Collections.sort(mapValues, Collections.reverseOrder());
        
        LinkedHashMap<String , Float> sortedMap = new LinkedHashMap<>();
        LinkedHashMap<String , Float> finalMap = new LinkedHashMap<>();
        Iterator<Float> valueIt = mapValues.iterator();
        
        while(valueIt.hasNext() ){
            float val = valueIt.next();
            Iterator<String> keyIt = mapKeys.iterator();
            
            while(keyIt.hasNext()){
                String key = keyIt.next();
                float comp1 = passedMap.get(key);
                float comp2 = val;
                
                if(comp1 == comp2){
                    keyIt.remove();
                    sortedMap.put(key, val);
                    
                    break;
                }
            }
        }
        int counter = 0;
        
        Iterator it = sortedMap.entrySet().iterator();
        while(it.hasNext() &&  counter<size  ){
            Map.Entry pair = (Map.Entry) it.next();
            finalMap.put((String)pair.getKey(), (Float)pair.getValue());
            counter++;
            it.remove();
            
        }
        return finalMap;
    }
    
    protected ArrayList<String> getRelatedDocsToQuery(String queryId) {
        ArrayList<String> docsTitle = new ArrayList<>();
        for (Pair p : queryDocRelatedness) {
            if(p.component1().toString().equalsIgnoreCase(queryId)){
                docsTitle.add(p.component2().toString());
            }
        }
        return docsTitle;
    }
    
    protected HashSet<String> getTitlesOfQueryEntities(String queryId) {
        HashSet<String> TitlesOfQueryEntities = new HashSet<>();
        for(String qEntity:queriesEntity.get(queryId).split(",") ){
            qEntity = qEntity.trim();
            if(entityTitle.containsKey(qEntity)){
                String qtitle = entityTitle.get(qEntity);
                TitlesOfQueryEntities.add(qtitle);
                if(collection.containsKey(qtitle)){
                    collection.put(qtitle, collection.get(qtitle)+1);
                    
                }
                else{
                    collection.put(qtitle, 1);
                }
                try{
                    Map<String,Float> qvector = SerializeHashMap.deserializeHashMap(qtitle);
                    colelementVectors.put(qtitle,qvector);
                }
                catch(Exception ex){
                    System.out.println(queryId + " does not have any entities!");
                }
            }

        }
        return TitlesOfQueryEntities;
    }
    
    protected double calculateP_e_d( String docTitle, String entityTitle) {
        
        double result = 0;
        float value = 0;
        try{
            HashMap<String, Integer> docsRelatedHash = docsrelated.get(docTitle);

            HashMap<String,Float> eRelated = getEntityVector(entityTitle, privatevectorSize);
            for (String e : eRelated.keySet()) {
                if(docsRelatedHash.containsKey(e)){
                    value += ee.CalcSimilarity(entityTitle, e)* docsRelatedHash.get(e);
//                    System.out.println("value for "+e+", "+entityTitle+" is: "+value);
                }
            }
            double up = value;
            double down = 0;
            if(!p_q_d_Down_ForALLRelatedDocs.containsKey(docTitle)){
                for(String re :docsRelatedHash.keySet()){
                    try{
                        double value2 = 0;
                        eRelated = getEntityVector(re, publicvectorSize);
                        for (String e:eRelated.keySet()) {
                            if(docsRelatedHash.containsKey(e)){
                                value2 += ee.CalcSimilarity(re, e)* docsRelatedHash.get(e);
                            }
                        }
                        down += value2 * docsRelatedHash.get(re) ;
                    }
                    catch(Exception ex){
                        
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();        
                    }
                }
                p_q_d_Down_ForALLRelatedDocs.put(docTitle, down);  
            }
            else{
                down = p_q_d_Down_ForALLRelatedDocs.get(docTitle);
            }
            result = up / down;
        }
        catch(Exception ex){
            
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            result = 0;
        }
        System.out.println("result for  "+docTitle+" and entity " + entityTitle +" is: "+result);
        return result;
    } 
    
    protected double calculateP_e_C( String entityTitle) {

        double result = 0;
        float value = 0;
        double up = 0;
        try{
            if(!p_e_C_up_ForALLEntities.containsKey(entityTitle)){
                HashMap<String,Float> eRelated = getEntityVector(entityTitle, privatevectorSize);

                for (String e:eRelated.keySet()) {
                    if(collection.containsKey(e)){
                        value += ee.CalcSimilarity(entityTitle, e)* collection.get(e);
//                        System.out.println("value for "+e+", "+entityTitle+" is: "+value);
                    }
                }
                up = value;
                p_e_C_up_ForALLEntities.put(entityTitle, up);
           }
           else{
                up = p_e_C_up_ForALLEntities.get(entityTitle);
           }
            
            double down = 0;
            if(p_e_C_Down == 0){
                for(String re :collection.keySet()){
                    try{
                        HashMap<String,Float> eRelated = getEntityVector(re, publicvectorSize);
                        double value2 = 0;
                        for (String e : eRelated.keySet()) {

                            if(collection.containsKey(e))
                                value2 += ee.CalcSimilarity(re, e)* collection.get(e);
                        }
                        down += value2 * collection.get(re);

                    }
                    catch(Exception ex){
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
                        result = 0;
                    }

                }
                p_e_C_Down=down;  
            }
            else{
                down = p_e_C_Down;
            }
            result = up / down;
        }
        catch(Exception ex){

            System.out.println(ex.getMessage());
            ex.printStackTrace();
            result = 0;
        }
        System.out.println("result for  collection and entity " + entityTitle +" is: "+result);
        return result;
    } 
    
    protected double calculateP_q_d(String docTitle, HashSet<String> TitlesOfQueryEntities) {
        double p_q_d = 1;
        for (String qt :TitlesOfQueryEntities) {
            double value = calculateP_e_d_C(docTitle,qt);
            if(value != 0)
                p_q_d *= value;
        }
        if(p_q_d == 1)
            return 0 ;
        return p_q_d;
    }

    protected double calculateP_q_C(HashSet<String> TitlesOfQueryEntities) {
        double p_q_C = 1;
        for (String qt :TitlesOfQueryEntities) {
            double value = calculateP_e_C(qt);
            if(value != 0)
                p_q_C *= value;
        }
         if(p_q_C == 1)
            return 0 ;
        return p_q_C;
    }
    
    protected double calculateP_q_D( HashSet<String> TitlesOfQueryEntities,  ArrayList<String> docsTitle) {
        double result = 0;
        try{
            if(p_q_d_ForALLRelatedDocs.isEmpty()){
                for (String docTitle : docsTitle) {
                    
                    double p_q_d = calculateP_q_d(docTitle, TitlesOfQueryEntities);
                    p_q_d_ForALLRelatedDocs.put(docTitle, p_q_d);
                }
            }
            for (String docTitle : docsTitle) {
                result += p_q_d_ForALLRelatedDocs.get(docTitle);
            }
        }catch(Exception ex){
            
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            
        }
        return result;
    }
    
    protected double calculateP_e_q(HashSet<String> titlesOfQueryEntities, ArrayList<String> docsTitle,String entityName) {
        double result = 0;
        try{
            calculateP_q_D(titlesOfQueryEntities, docsTitle);
            
            for (String docTitle : docsTitle) {
                double p_e_d_C = calculateP_e_d_C(docTitle,entityName);
                result += ( p_e_d_C * p_q_d_ForALLRelatedDocs.get(docTitle));
                
            }
        }catch(Exception ex){
            
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            
        }
        return result;
    }
    
    protected double calculateP_e_d_C( String docTitle, String entityName) {
        
        double p_e_d = calculateP_e_d(docTitle, entityName);
        double p_e_C = calculateP_e_C(entityName);
        double result = (0.6 * p_e_d)+(0.4 * p_e_C);
        return result;
    }
}
