/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package featurs;

import baselines.Clarity;
import baselines.PerformancePredictor;
import baselines.UEF;
import core.SqlConnection;
import static featurs.Feature.entityTitle;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.utility.Parameters;

/**
 *
 * @author parastoo
 */
public class F07_UEF_clarityBased extends Feature{

    public F07_UEF_clarityBased(String dataset, String rs, int docCount, int start, int end, String feature) {
        super(dataset, rs, docCount, start, end, feature);
    }

    @Override
    public void run() throws IOException, SQLException{
        
            createEntitiyTitle();
            for (int i =start; i<=end; i++) {
                try {
                    Parameters query = getQuery(i);
                    String ScorePath ="./crossfold/"+dataset+"/result/"+this.rs+"-"+dataset+"-"+this.docCount+"docs-percentEntities-F03.txt";
                    String QueryIndex = "C:\\Users\\parastoo\\Documents\\NetBeansProjects\\explicit-semantic-analysis-master\\index\\" + dataset + "\\"+i+"\\";
                    Retrieval QueryRetrieval = RetrievalFactory.instance(Arrays.asList(QueryIndex), Parameters.create());
                    query.set("fbTerm", 1000);
                    query.set("fbOrigWeight", 0.5);
                    query.set("fbDocs", 20);
                    query.set("numDocs", 1000);
                    query.get("sampleSize", 100);
                    query.set("StdevApproach", "fixed");
                    query.set("NormalizeStdev", false);
                    String queryText = query.getString("text");
                    PerformancePredictor qpp = new UEF(QueryRetrieval,ScorePath);
                    double score = qpp.predict(queryText, query);

                    predictions.put(""+i,score);
                    resultwriter.write(i+ " "+ score);
                    resultwriter.write("\n");

                    resultwriter.flush();

                } catch (Exception ex) {
                    
                    System.out.println(ex.getMessage());
                    ex.printStackTrace();
                }
            }
            resultwriter.close();
         
    }
    
    private Parameters getQuery(int i) throws SQLException {
        Parameters queryContent = Parameters.create();
        String query = "SELECT q.id,q.entity,q.title from query_"+dataset+" q where q.id="+i; 
        ResultSet resultset = SqlConnection.select(query);
        while(resultset.next()){
            String number = ""+i;
            String text = resultset.getString(3);
            String[] entities = resultset.getString(2).split(",");
            for (String entry: entities) {
                entry = entry.trim();
                if(entityTitle.containsKey(entry) && !entry.equals("") && !entry.equals("\\s")){
                    text += " " + entityTitle.get(entry);
                }
            }
            queryContent.set("number",number);
            queryContent.set("text",text);
        }
        return queryContent;
    }
}
