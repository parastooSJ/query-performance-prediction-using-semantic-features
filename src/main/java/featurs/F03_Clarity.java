/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package featurs;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;


/**
 *
 * @author Parastoo
 */
public class F03_Clarity extends Feature{
    public F03_Clarity(String dataset, String rs, int docCount, int start, int end, String feature) {
        super(dataset, rs, docCount, start, end, feature);
    }
    
    @Override
    public void run() throws IOException, SQLException{
        
            
            createEntitiyTitle();
            createCollection();


            for (int i =start; i<=end; i++) {
                p_q_d_ForALLRelatedDocs = new HashMap<>();
                p_q_d_Down_ForALLRelatedDocs = new HashMap<>();
                CalculateSimilarityScore(Integer.toString(i));

            }
            resultwriter.close();
        
    }
     
    private double CalculateSimilarityScore(String queryId) {
        double score = 0;
        try {
            ArrayList<String> docsTitle = getRelatedDocsToQuery(queryId);
            HashSet<String> titlesOfQueryEntities = getTitlesOfQueryEntities(queryId);
            
            for (String colElement :collection.keySet()) {
                double tempraryScore = 0;

                if(titlesOfQueryEntities.size()>0){
                    double p_e_q = calculateP_e_q(titlesOfQueryEntities,docsTitle,colElement);
                    double p_e_C = calculateP_e_C(colElement);
                    if (p_e_C != 0 && p_e_q !=0 )
                        tempraryScore = p_e_q * Math.log(p_e_q/p_e_C);
                    if(Double.isNaN(tempraryScore) || Double.isInfinite(tempraryScore))
                        tempraryScore = 0;
                    score += tempraryScore;
                }
            }
            predictions.put(queryId,score);
            resultwriter.write(queryId+ " "+ score);
            resultwriter.write("\n");
            
            resultwriter.flush();
        } 
        catch (Exception ex) {
            
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       return score;
    
    }
}
