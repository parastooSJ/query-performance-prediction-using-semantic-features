/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package featurs;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author parastoo
 */
public class F04_InnerSimilarity extends Feature{

    public F04_InnerSimilarity(String dataset, String rs, int docCount, int start, int end, String feature) {
        super(dataset, rs, docCount, start, end, feature);
    }
    
    @Override
    public void run() throws IOException, SQLException{
        
            
            createEntitiyTitle();
            createCollection();
            for (int i =start; i<=end; i++) {
                p_q_d_ForALLRelatedDocs = new HashMap<>();
                p_q_d_Down_ForALLRelatedDocs = new HashMap<>();
                CalculateSimilarityScore(Integer.toString(i));

            }
            resultwriter.close();
        
    }
    
    private double CalculateSimilarityScore(String queryId) {
        double score = 0;
        try {
            ArrayList<String> docsTitle = getRelatedDocsToQuery(queryId);
            
            for (int i = 0; i < docsTitle.size(); i++) {
                for (int j = i+1; j < docsTitle.size(); j++) {
                    for (String colElement :docsrelated.get(docsTitle.get(i)).keySet()) {
                        double p_e_d = calculateP_e_d_C(docsTitle.get(j),colElement);
                        score += p_e_d;
                
                    }
                }
            }
            score = score / docsTitle.size();
            predictions.put(queryId,score);
            resultwriter.write(queryId+ " "+ score);
            resultwriter.write("\n");
            
            resultwriter.flush();
        } 
        catch (Exception ex) {
            
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        return score;
    }

    
}

