/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package featurs;

import static core.SerializeHashMap.filename;
import static core.SerializeHashMap.titleEntity;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import util.StandardDeviation;

/**
 *
 * @author Parastoo
 */
public class F01_NQC extends Feature {
    

    public F01_NQC(String dataset, String rs, int docCount, int start, int end, String feature) {
        super(dataset, rs, docCount, start, end, feature);
    }
        
    @Override
    public void run() throws IOException, SQLException{
    
            createEntitiyTitle();
            createCollection();
            
            for (int i =start; i<=end; i++) {
                p_q_d_ForALLRelatedDocs = new HashMap<>();
                p_q_d_Down_ForALLRelatedDocs = new HashMap<>();                                                                                                                                    
                CalculateSimilarityScore(Integer.toString(i));

            }
            resultwriter.close();
        
        
    }

    private double CalculateSimilarityScore(String queryId) {
        double score = 0;
        try {
            ArrayList<String> docsTitle = getRelatedDocsToQuery(queryId);
            HashSet<String> TitlesOfQueryEntities = getTitlesOfQueryEntities(queryId);
            ArrayList<Double> p_q_d_list = new ArrayList<>();
            
            for (int i = 0; i < docsTitle.size(); i++) {
                
                double p_q_d = calculateP_q_d(docsTitle.get(i),TitlesOfQueryEntities);
                p_q_d_list.add(p_q_d);
            }
            double stdev = StandardDeviation.calculateSD(p_q_d_list);
            System.out.println("stdev: "+stdev);
            double p_q_C = calculateP_q_C(TitlesOfQueryEntities);
            System.out.println("coll: "+p_q_C);
            if(stdev != 0)
                score = stdev * Math.log( 1/p_q_C);
            else
                score = 0;
            System.out.println("-------------------------------------------------------");
            predictions.put(queryId,score);
            resultwriter.write(queryId+ " "+ score);
            resultwriter.write("\n");
            resultwriter.flush();
        } 
        catch (Exception ex) {
            
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        return score;
    }

    

    

    
    
}
