/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.vanoosten.esa.tools;

import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.queryparser.classic.ParseException;
import be.vanoosten.esa.EnwikiFactory;
import be.vanoosten.esa.WikiAnalyzer;
import be.vanoosten.esa.WikiFactory;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import core.SqlConnection;
import static org.apache.lucene.util.Version.LUCENE_48;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohaddeseh
 */
public class ESAtester_EN {

    MysqlDataSource dataSource ;
    Connection connection;
    Statement statement;
    HashMap<String, Double> countMap;
    public ESAtester_EN(){
        countMap = new HashMap<>();
        dataSource = new MysqlDataSource();
        dataSource.setUser("root");
        dataSource.setPassword("137428sAm");
        dataSource.setServerName("localhost");
        dataSource.setDatabaseName("query_performance_prediction");
        dataSource.setUseUnicode(true);
        dataSource.setCharacterEncoding("UTF-8");

        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(ESAtester_EN.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) throws IOException, ParseException, Exception {

        SqlConnection.getInstance();
        ESAtester_EN esa = new ESAtester_EN();
        String n="or's gate";
        esa.CalcSimilarity(n,n);
        Map<String, Float> r1 = esa.getSimilartyVector(n);
        n=URLEncoder.encode(n, StandardCharsets.UTF_8.toString());
        for (String str : r1.keySet()) {
            System.out.println(str);
            
        }
    }

    public static double calcESA(String t1, String t2) throws Exception {
        WikiFactory factory = new EnwikiFactory();
        CharArraySet stopWords = factory.getStopWords();

        String loc_index = "C:\\Development\\esa\\enwiki";
        WikiAnalyzer wa = new WikiAnalyzer(LUCENE_48, stopWords);
        Vectorizer vec = new Vectorizer(new File(loc_index), wa);
        SemanticSimilarityTool sst = new SemanticSimilarityTool(vec);
        double sim = sst.findSemanticSimilarity(t1, t2);
        factory.close();
        return sim;
    }

   
    public double CalcSimilarity(String entity1, String entity2) {
        double esa=0;
        try {
            String entity1Fixed = entity1.replace("\"", "'");
            String entity2Fixed = entity2.replace("\"", "'");
                String query= "SELECT similarity FROM `similarityScore` WHERE (entity1 = \""+entity1Fixed+"\" and entity2 = \""+entity2Fixed+
                        "\" ) or (entity2 = \""+entity1Fixed+"\" and entity1 = \""+entity2Fixed+"\" )";
                ResultSet resultset = SqlConnection.select(query);
                if(resultset.next()){
                    esa= Double.parseDouble( resultset.getString(1));
                }
                else{
                    try{
                        esa = calcESA(entity1, entity2);
                    }
                    catch(Exception ex){
                        esa = calcESA(fix(entity1), fix(entity2));
                    }
                    
                    try{ 
                            PreparedStatement updateSales = connection.prepareStatement(
                                "insert into similarityScore (entity1,entity2,similarity) values(?,?,?)");
                            updateSales.setString(1, entity1); 
                            updateSales.setString(2, entity2);
                            updateSales.setString(3, Double.toString(esa));
                            updateSales.executeUpdate();

                        }
                        catch(Exception ex){
                            statement.close();
                            connection.close();
                            connection = dataSource.getConnection();
                            statement = connection.createStatement();
                        }
                }
                
              
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        return esa;
    }

    public Map<String, Float> getSimilartyVector(String element) throws IOException, ParseException {
        
        WikiFactory factory = new EnwikiFactory();
        CharArraySet stopWords = factory.getStopWords();

        String loc_index = "C:\\Development\\esa\\enwiki";
        WikiAnalyzer wa = new WikiAnalyzer(LUCENE_48, stopWords);
        Vectorizer vec = new Vectorizer(new File(loc_index), wa);

        SemanticSimilarityTool sst = new SemanticSimilarityTool(vec);
        try{
            return sst.findSimilarityVector(element);
        }
        catch(Exception ex){
            return sst.findSimilarityVector(fix(element));
        }
    }
    private String fix(String str){
        String fixed = str.replaceAll("\\W+", " ");
        fixed = fixed.trim();
        fixed = fixed.toLowerCase();
        return fixed;
    }

    public Map<String,Float> getVector(String t1) throws Exception {
        WikiFactory factory = new EnwikiFactory();
        CharArraySet stopWords = factory.getStopWords();

        String loc_index = "C:\\Development\\esa\\enwiki";
        WikiAnalyzer wa = new WikiAnalyzer(LUCENE_48, stopWords);
        Vectorizer vec = new Vectorizer(new File(loc_index), wa);

        SemanticSimilarityTool sst = new SemanticSimilarityTool(vec);

        Map<String,Float> result = sst.findSimilarityVector(t1);
        factory.close();
        return result;
    }
}
