/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ 
package query_performance_prediction;

import com.github.andrewoma.dexx.collection.Pair;
import core.SerializeHashMap;
import core.SqlConnection;
import featurs.*;
import java.io.*;
import java.sql.*;
import java.util.*;


/**
 *
 * @author Parastoo
 */
public class Query_performance_prediction {

    /**
     * @param args the command line arguments
     */
    private final static Object lock = new Object();
    public static void main(String[] args) throws FileNotFoundException, IOException, SQLException {
        
                
        SqlConnection.getInstance();
        Query_performance_prediction  q = new Query_performance_prediction(); 
       
        String dataset = "2009";
        String rs= "ql";
        int docCount = 10;
        int start =1 ;
        int end = 200 ;
        float rate = 1.0f;
        boolean avg = false;
//        SerializeHashMap se = new SerializeHashMap(dataset, rs, docCount, start, end);
//        se.serun();
        
        F01_NQC f1_2009 = new F01_NQC(dataset, rs, docCount, start, end, "01");
        F02_Similarity f2_2009 = new F02_Similarity(dataset, rs, docCount, start, end, "02");
        F03_Clarity f3_2009 = new F03_Clarity(dataset, rs, docCount, start, end, "03");
        F04_InnerSimilarity f4_2009 = new F04_InnerSimilarity(dataset, rs, docCount, start, end, "04");
        F05_Clarity_AbstractBased f5_2009 = new F05_Clarity_AbstractBased(dataset, rs, docCount, start, end, "05");
        F06_WIG f6_2009 = new F06_WIG(dataset, rs, docCount, start, end, "06");
        
        q.test(rs, dataset, docCount, start, end, rate, avg, "01",f1_2009);
        q.test(rs, dataset, docCount, start, end, rate, avg, "02",f2_2009);
        q.test(rs, dataset, docCount, start, end, rate, avg, "03",f3_2009);
        q.test(rs, dataset, docCount, start, end, rate, avg, "04",f4_2009);
        q.test(rs, dataset, docCount, start, end, rate, avg, "05",f5_2009);
        q.test(rs, dataset, docCount, start, end, rate, avg, "06",f6_2009);

        
        dataset = "2012";
        rs= "ql";
        docCount = 10;
        start = 201;
        end = 250;
        rate = 1.0f;
        avg=false;
        
        F01_NQC f1_2012 = new F01_NQC(dataset, rs, docCount, start, end, "01");
        F02_Similarity f2_2012 = new F02_Similarity(dataset, rs, docCount, start, end, "02");
        F03_Clarity f3_2012 = new F03_Clarity(dataset, rs, docCount, start, end, "03");
        F04_InnerSimilarity f4_2012 = new F04_InnerSimilarity(dataset, rs, docCount, start, end, "04");
        F05_Clarity_AbstractBased f5_2012 = new F05_Clarity_AbstractBased(dataset, rs, docCount, start, end, "05");
        F06_WIG f6_2012 = new F06_WIG(dataset, rs, docCount, start, end, "06");
        
        q.test(rs, dataset, docCount, start, end, rate, avg, "01",f1_2012);
        q.test(rs, dataset, docCount, start, end, rate, avg, "02",f2_2012);
        q.test(rs, dataset, docCount, start, end, rate, avg, "03",f3_2012);
        q.test(rs, dataset, docCount, start, end, rate, avg, "04",f4_2012);
        q.test(rs, dataset, docCount, start, end, rate, avg, "05",f5_2012);
        q.test(rs, dataset, docCount, start, end, rate, avg, "06",f6_2012);
        
        
        dataset = "robust";
        rs= "ql";
        docCount = 10;
        start =301 ;
        end = 700;
        rate =1.0f;
        avg=false;
        
        F01_NQC f1_robust = new F01_NQC(dataset, rs, docCount, start, end, "01");
        F02_Similarity f2_robust = new F02_Similarity(dataset, rs, docCount, start, end, "02");
        F03_Clarity f3_robust = new F03_Clarity(dataset, rs, docCount, start, end, "03");
        F04_InnerSimilarity f4_robust = new F04_InnerSimilarity(dataset, rs, docCount, start, end, "04");
        F05_Clarity_AbstractBased f5_robust = new F05_Clarity_AbstractBased(dataset, rs, docCount, start, end, "05");
        F06_WIG f6_robust = new F06_WIG(dataset, rs, docCount, start, end, "06");
        
        q.test(rs, dataset, docCount, start, end, rate, avg, "01",f1_robust);
        q.test(rs, dataset, docCount, start, end, rate, avg, "02",f2_robust);
        q.test(rs, dataset, docCount, start, end, rate, avg, "03",f3_robust);
        q.test(rs, dataset, docCount, start, end, rate, avg, "04",f4_robust);
        q.test(rs, dataset, docCount, start, end, rate, avg, "05",f5_robust);
        q.test(rs, dataset, docCount, start, end, rate, avg, "06",f6_robust);

    }
    
    public static HashMap<String, Double> readPerfFromFils(String input) throws FileNotFoundException {
        HashMap<String, Double> result = new HashMap<>();
        Scanner in = new Scanner(new File(input));
        while (in.hasNextLine()){
            try{
            String qid = in.next();
            double val = in.nextDouble();
            result.put(qid,val);
            }
            catch(Exception ex){
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        in.close();
        return result;
    }
    
    public static HashMap<String, Double> readPerfFromFile (String filename, String targetMetric) throws FileNotFoundException {
        HashMap<String, Double> result = new HashMap<>();
        Scanner in = new Scanner(new File(filename));
        while (in.hasNextLine()){
            String metric = in.next();
            String qid = in.next();
            if (qid.equals("all"))
                break;
            double val = in.nextDouble();
            if (metric.equals(targetMetric))
                result.put(qid,val);
        }
        in.close();
        return result;
    }
    
    public static double [] evalAll(List <Pair<String, Double>> a, List <Pair<String, Double>> b) {
        double[] result = new double[3];
        result[0] = computeSpearmannCorrelation(a, b);
        result[1] = computePearsonCorrelation(a, b);
        result[2] = computeKendallsTauDistance(a, b);
        return  result;
    }
    
    public static double computeSpearmannCorrelation(List <Pair<String, Double>> a, List <Pair<String, Double>> b) {
        Comparator comparator = new Comparator<Pair<String, Double>>(){
            @Override
            public int compare(Pair<String, Double> p1, Pair<String, Double> p2) {
                return p2.component2().compareTo(p1.component2());
            }
        };
        Collections.sort(a, comparator);
        Collections.sort(b, comparator);

        Map <String, Integer> first = getMapRank(a);
        for (int i=0; i<a.size(); i++) {
            String q_i = a.get(i).component1();

        }
        for (int i=0; i<b.size(); i++) {
            String q_i = b.get(i).component1();

        }
        Map <String, Integer> second = getMapRank(b);

        double cov = 0, var1 = 0, var2 = 0;
        for (int i=0; i<a.size(); i++) {
            for (int j=i+1; j<a.size(); j++) {
                String q_i = a.get(i).component1();
                String q_j = a.get(j).component1();
                cov += ((first.get(q_i) - first.get(q_j)) * (second.get(q_i) - second.get(q_j)));
                var1 += ((first.get(q_i) - first.get(q_j)) * (first.get(q_i) - first.get(q_j)));
                var2 += ((second.get(q_i) - second.get(q_j)) * (second.get(q_i) - second.get(q_j)));
            }
        }
        return cov / Math.sqrt(var1 * var2);
    }
    
    public static double computePearsonCorrelation(List <Pair<String, Double>> a, List <Pair<String, Double>> b) {
        Map <String, Double> first = pairToMap(a);
        Map <String, Double> second = pairToMap(b);

        double cov = 0, var1 = 0, var2 = 0;
        for (int i=0; i<a.size(); i++) {
            for (int j=i+1; j<a.size(); j++) {
                String q_i = a.get(i).component1();
                String q_j = a.get(j).component1();
                cov += ((first.get(q_i) - first.get(q_j)) * (second.get(q_i) - second.get(q_j)));
                var1 += ((first.get(q_i) - first.get(q_j)) * (first.get(q_i) - first.get(q_j)));
                var2 += ((second.get(q_i) - second.get(q_j)) * (second.get(q_i) - second.get(q_j)));
            }
        }
        return cov / Math.sqrt(var1 * var2);
    }
    
    public static double computeKendallsTauDistance(List <Pair<String, Double>> a, List <Pair<String, Double>> b) {
        Map <String, Double> first = pairToMap(a);
        Map <String, Double> second = pairToMap(b);

        int concordant = 0, discordant = 0;
        for (int i=0; i<a.size(); i++) {
            for (int j=i+1; j<a.size(); j++) {
                String q_i = a.get(i).component1();
                String q_j = a.get(j).component1();
                if ((first.get(q_i) - first.get(q_j))*(second.get(q_i) - second.get(q_j)) < 0)
                    discordant++;
                if ((first.get(q_i) - first.get(q_j))*(second.get(q_i) - second.get(q_j)) > 0)
                    concordant++;
            }
        }
        double z = (double)(a.size()*(a.size()-1))/2.;
        return (double)(concordant-discordant)/z;
    }
    
    public static Map <String, Integer> getMapRank (List <Pair<String, Double>> sortedList) {
        Map <String, Integer> result = new HashMap <> ();
        int rank = 0;
        double previousScore = -1000;
        for (int i=0; i<sortedList.size(); i++) {
            if (sortedList.get(i).component2() != previousScore) {
                rank++;
                previousScore = sortedList.get(i).component2();
            }
            result.put(sortedList.get(i).component1(), rank);
        }
        return result;
    }
    
    public static Map <String, Double> pairToMap (List <Pair<String, Double>> list) {
        Map <String, Double> result = new HashMap <> ();
        for (int i=0; i<list.size(); i++) {
            result.put(list.get(i).component1(), list.get(i).component2());
        }
        return result;
    }
    
    public void test(String rs, String dataset, int documentNumber, int start,int end,float rate, boolean avg, String fname,Feature f){
        
        try {
            String resultPath = "./crossfold/"+dataset+"/correlation/correlation-"+rs+"-"+dataset+"F"+fname+".txt";
            BufferedWriter bw = new BufferedWriter( new OutputStreamWriter(
                      new FileOutputStream(resultPath), "UTF8"));
            String mapFile = "./crossfold/"+dataset+"/map/map-"+rs+"-"+dataset+".txt";
            
            HashMap<String, Double> map =  Query_performance_prediction.readPerfFromFile(mapFile,"map");
            f.run();
            List <Pair<String, Double>> trainMap = new ArrayList<>();
            HashMap<String,Double> predictions = Query_performance_prediction.readPerfFromFils(f.filename);
            List <Pair<String, Double>> trainSet = new ArrayList<>();
            
            for(String k:predictions.keySet()){
                double score = 0;
                score = predictions.get(k);
                trainSet.add(new Pair<String,Double>(k,score));
            }
            for(Pair p : trainSet){
                if(map.containsKey(p.component1().toString())){
                    double value = map.get(p.component1().toString());
                    String key = p.component1().toString();
                    trainMap.add(new Pair<String,Double>(key,value));
                }
                else{
                    trainMap.add(new Pair<String,Double>(p.component1().toString(),0.0));
                }
            }
            double pearson = Query_performance_prediction.evalAll(trainMap, trainSet)[1];
            double kandal  = Query_performance_prediction.evalAll(trainMap, trainSet)[2];
            double spearman = Query_performance_prediction.evalAll(trainMap, trainSet)[2];
           
            bw.write("pearson is: "+pearson+"\n");
            bw.write("kandal is:"+kandal+"\n");
            bw.write("spearman is:"+spearman+"\n");

            bw.close();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
    
    
    
}

    

